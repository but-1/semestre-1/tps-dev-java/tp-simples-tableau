public class MaClass {
    MaClass() {
    }

    public static void main(String[] var0) {
        if (var0.length != 1) {
            System.out.println("Usage : java MaClass key");
            System.exit(0);
        }

        String var1 = var0[0];
        if (var1.length() == 13 && var1.charAt(2) == 'v' && var1.charAt(0) == 'j') {
            boolean authenticationGranted = true;
            for (int var2 = 0; var2 < var1.length(); ++var2) {
                if (var2 != 0 && var2 != 2 && var1.charAt(var2) != 'v') {
                    authenticationGranted = false;
                    break;
                }
            }
            if (authenticationGranted) {
                System.out.println("Authentication granted !");
                System.exit(0);
            }
        }

        System.out.print("You are not the one I'm looking for. GET OUT OF HERE !");
    }
}
